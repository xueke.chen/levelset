import sys
import os
import numpy as np

# Get the parent directory
parent_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

# Append the parent directory to sys.path
sys.path.append(parent_dir)

#now we can import the module integration_methods from the parent dir
from integration_methods import (
    find_approximate_zeros, find_intervals, trapezoidal_rule, integrate_1d,
    create_uniform_grid, find_valid_cells, integrate_over_cells, integrate_over_cells_quadratic,
    create_uniform_grid_3d, find_valid_cells_3d, integrate_over_cells_trilinear,
    monte_carlo_integration, integrate_using_moments, integrate_using_sub_triangulation,
    find_boundary_points, parametrize_boundary, integrate_over_domain
)

import scipy.integrate as integrate

def expected_integral_value():
    f = lambda x, y: x + y + 1
    g = lambda x, y: x**2 + y**2 - 1

    result, _ = integrate.dblquad(lambda x, y: f(x, y) if g(x, y) > 0 else 0, -1, 1, lambda x: -1, lambda x: 1)
    return result


import unittest

class TestIntegrationMethods(unittest.TestCase):

    def test_find_approximate_zeros(self):
        g = lambda x: np.cos(x) - 0.5
        zeros = find_approximate_zeros(g, 0, 2 * np.pi, 100)
        self.assertAlmostEqual(zeros[0], np.pi / 3, places=1)

    def test_trapezoidal_rule(self):
        f = lambda x: np.sin(x)
        integral, _, _ = trapezoidal_rule(f, 0, np.pi, 100)
        self.assertAlmostEqual(integral, 2, places=1)

    def test_integrate_1d(self):
        f = lambda x: np.sin(x)
        g = lambda x: np.cos(x) - 0.5
        integral, _ = integrate_1d(f, g, 0, 2 * np.pi)
        self.assertAlmostEqual(integral, 0, places=1)

    def test_integrate_over_cells(self):
        f = lambda x, y: x + y + 1
        g = lambda x, y: x**2 + y**2 - 1
        a, b, c, d = -1, 1, -1, 1
        num_points = 100
        X, Y = create_uniform_grid(a, b, c, d, num_points)
        valid_cells = find_valid_cells(g, X, Y)
        integral = integrate_over_cells(f, valid_cells)
        self.assertAlmostEqual(integral, 0.95, places=2)

    def test_integrate_over_cells_quadratic(self):
        f = lambda x, y: x + y + 1
        g = lambda x, y: x**2 + y**2 - 1
        a, b, c, d = -1, 1, -1, 1
        num_points = 100
        X, Y = create_uniform_grid(a, b, c, d, num_points)
        valid_cells = find_valid_cells(g, X, Y)
        integral = integrate_over_cells_quadratic(f, valid_cells)
        self.assertAlmostEqual(integral, 0.97, places=2)

    def test_integrate_over_cells_trilinear(self):
        f = lambda x, y, z: x + y + z + 1
        g = lambda x, y, z: x**2 + y**2 + z**2 - 1
        a, b, c, d, p, q = -1, 1, -1, 1, -1, 1
        num_points = 5
        X, Y, Z = create_uniform_grid_3d(a, b, c, d, p, q, num_points)
        valid_cells = find_valid_cells_3d(g, X, Y, Z)
        integral = integrate_over_cells_trilinear(f, valid_cells)
        self.assertAlmostEqual(integral, 2.1875, places=2)

    def test_monte_carlo_integration(self):
        f = lambda x, y: x + y + 1
        g = lambda x, y: x**2 + y**2 - 1
        a, b, c, d = -1, 1, -1, 1
        num_points = 1000000  # Further increased number of sample points
        integral = monte_carlo_integration(f, g, a, b, c, d, num_points, seed=42)  # Set seed for reproducibility
        expected_value = expected_integral_value()
        self.assertAlmostEqual(integral, expected_value, places=2)


    def test_integrate_using_moments(self):
        f = lambda x, y: x + y + 1
        g = lambda x, y: x**2 + y**2 - 1
        a, b, c, d = -1, 1, -1, 1
        num_points = 100
        X, Y = create_uniform_grid(a, b, c, d, num_points)
        valid_cells = find_valid_cells(g, X, Y)
        integral = integrate_using_moments(f, valid_cells)
        self.assertAlmostEqual(integral, 0.95, places=2)

    def test_integrate_using_sub_triangulation(self):
        f = lambda x, y: x + y + 1
        g = lambda x, y: x**2 + y**2 - 1
        a, b, c, d = -1, 1, -1, 1
        num_points = 100
        X, Y = create_uniform_grid(a, b, c, d, num_points)
        valid_cells = find_valid_cells(g, X, Y)
        integral = integrate_using_sub_triangulation(f, valid_cells)
        self.assertAlmostEqual(integral, 0.95, places=2)

    def test_local_parametrization(self):
        f = lambda x, y: x + y 
        g = lambda x, y: x**2 + y**2 - 1
        a, b, c, d = -1, 1, -1, 1
        num_points = 100
        X, Y = create_uniform_grid(a, b, c, d, num_points)
        boundary_points = find_boundary_points(X, Y, g)
        x, y = parametrize_boundary(boundary_points)
        integral = integrate_over_domain(f, X, Y, g, x, y)
        expected_value = expected_integral_value()
        self.assertAlmostEqual(integral,0, places=2)

if __name__ == "__main__":
    unittest.main()
