import numpy as np

def find_approximate_zeros(g, a, b, num_points=100):
    """
    Find approximate zeros of the function g within the interval [a, b].
    """
    x = np.linspace(a, b, num_points)
    zeros = []
    for i in range(len(x) - 1):
        if g(x[i]) * g(x[i + 1]) <= 0:
            zero = (x[i] + x[i + 1]) / 2.0
            zeros.append(zero)
    return zeros

def find_intervals(g, a, b, num_points=100):
    """
    Find intervals within [a, b] where g(x) > 0.
    """
    zeros = find_approximate_zeros(g, a, b, num_points)
    intervals = []
    if g(a) > 0:
        intervals.append((a, zeros[0]))
    for i in range(len(zeros) - 1):
        mid_point = (zeros[i] + zeros[i + 1]) / 2
        if g(mid_point) > 0:
            intervals.append((zeros[i], zeros[i + 1]))
    if g(b) > 0:
        intervals.append((zeros[-1], b))
    return intervals

def trapezoidal_rule(f, p, q, n):
    """
    Approximate the integral of f(x) over [p, q] using the trapezoidal rule with n intervals.
    """
    h = (q - p) / n
    x_points = np.linspace(p, q, n + 1)
    integral = 0.5 * (f(p) + f(q))
    for i in range(1, n):
        integral += f(p + i * h)
    integral *= h
    return integral, x_points, f(x_points)

def integrate_1d(f, g, a, b, num_points=100, n_intervals=10):
    """
    Integrate f(x) over the intervals where g(x) > 0 within [a, b].
    """
    intervals = find_intervals(g, a, b, num_points)
    total_integral = 0
    trapezoids = []
    for interval in intervals:
        integral, x_points, y_points = trapezoidal_rule(f, interval[0], interval[1], n_intervals)
        total_integral += integral
        trapezoids.append((x_points, y_points))
    return total_integral, trapezoids
