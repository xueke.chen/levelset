import numpy as np

def monte_carlo_integration(f, g, a, b, c, d, n=100, seed=None):
    """
    Calculate the integral of f(x, y) over the domain where g(x, y) > 0 using Monte-Carlo integration.
    """
    if seed is not None:
        np.random.seed(seed)
    
    # Generate random points within the bounding box
    x_random = np.random.uniform(a, b, n)
    y_random = np.random.uniform(c, d, n)

    # Evaluate g(x, y) and f(x, y) at these points
    inside_omega = g(x_random, y_random) > 0
    if np.sum(inside_omega) == 0:
        return 0  # Avoid division by zero

    # Calculate the integral as the mean value of f inside the domain times the area of the domain
    integral_estimate = np.mean(f(x_random[inside_omega], y_random[inside_omega])) * (b - a) * (d - c) * np.mean(inside_omega)
    return integral_estimate
