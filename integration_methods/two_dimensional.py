import numpy as np

def create_uniform_grid(a, b, c, d, num_points):
    """
    Create a uniform grid over the domain [a, b] x [c, d].
    """
    x = np.linspace(a, b, num_points)
    y = np.linspace(c, d, num_points)
    X, Y = np.meshgrid(x, y)
    return X, Y

def find_valid_cells(g, X, Y):
    """
    Identify the cells where g(x, y) > 0.
    """
    valid_cells = []
    for i in range(X.shape[0] - 1):
        for j in range(X.shape[1] - 1):
            if (g(X[i, j], Y[i, j]) > 0 or g(X[i + 1, j], Y[i + 1, j]) > 0 or
                g(X[i, j + 1], Y[i, j + 1]) > 0 or g(X[i + 1, j + 1], Y[i + 1, j + 1]) > 0):
                valid_cells.append(((X[i, j], Y[i, j]), (X[i + 1, j], Y[i + 1, j]), 
                                    (X[i, j + 1], Y[i, j + 1]), (X[i + 1, j + 1], Y[i + 1, j + 1])))
    return valid_cells

def integrate_bilinear(f0, f1, f2, f3, y0, y1, x0, x2):
    """
    Approximate the integral over each valid cell using bilinear interpolation.
    """
    A = f0
    B = f1 - f0
    C = f2 - f0
    D = f3 - f2 - f1 + f0
   
    integral = (A + B/2 + C/2 + D/4) * (y1 - y0) * (x2 - x0)
    return integral

def integrate_over_cells(f, valid_cells):
    """
    Integrate f over all valid cells using bilinear interpolation.
    """
    total_integral = 0
    for cell in valid_cells:
        x0, y0 = cell[0]
        x1, y1 = cell[1]
        x2, y2 = cell[2]
        x3, y3 = cell[3]
        
        f0 = f(x0, y0)
        f1 = f(x1, y1)
        f2 = f(x2, y2)
        f3 = f(x3, y3)
        
        integral = integrate_bilinear(f0, f1, f2, f3, y0, y1, x0, x2)
        total_integral += integral
    return total_integral

def integrate_over_cells_quadratic(f, valid_cells):
    """
    Integrate f over all valid cells using higher-order polynomial approximation.
    """
    total_integral = 0
    for cell in valid_cells:
        integral = integrate_quadratic(f, cell)
        total_integral += integral
    return total_integral

def integrate_quadratic(f, cell):
    """
    Approximate the integral using quadratic polynomial interpolation.
    """
    x0, y0 = cell[0]
    x1, y1 = cell[1]
    x2, y2 = cell[2]
    x3, y3 = cell[3]

    xm = (x0 + x1 + x2 + x3) / 4
    ym = (y0 + y1 + y2 + y3) / 4

    x01, y01 = (x0 + x1) / 2, (y0 + y1) / 2
    x02, y02 = (x0 + x2) / 2, (y0 + y2) / 2

    f0 = f(x0, y0)
    f1 = f(x1, y1)
    f2 = f(x2, y2)
    f3 = f(x3, y3)
    f01 = f(x01, y01)
    f02 = f(x02, y02)
    
    A_matrix = np.array([
        [1, x0, y0, x0**2, y0**2, x0*y0],
        [1, x1, y1, x1**2, y1**2, x1*y1],
        [1, x2, y2, x2**2, y2**2, x2*y2],
        [1, x3, y3, x3**2, y3**2, x3*y3],
        [1, x01, y01, x01**2, y01**2, x01*y01],
        [1, x02, y02, x02**2, y02**2, x02*y02],
    ])
    
    f_values = np.array([f0, f1, f2, f3, f01, f02])
    coefficients = np.linalg.solve(A_matrix, f_values)
    A, B, C, D, E, F = coefficients

    integral = (A + B/2*(x2-x0) + C/2*(y1-y0) + D/3*(x2-x0)**2 + E/3*(y1-y0)**2 + F/4*(x2-x0)*(y1-y0)) * (x2-x0) * (y1-y0)
    return integral
