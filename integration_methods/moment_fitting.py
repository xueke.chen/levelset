def calculate_cell_moments(vertices):
    """
    Calculate moments for each cell.
    """
    x0, y0 = vertices[0]
    x1, y1 = vertices[1]
    x2, y2 = vertices[2]
    x3, y3 = vertices[3]

    area = (x2 - x0) * (y1 - y0)
    centroid_x = (x0 + x1 + x2 + x3) / 4
    centroid_y = (y0 + y1 + y2 + y3) / 4
    return area, centroid_x, centroid_y

def integrate_using_moments(f, valid_cells):
    """
    Integrate using moments.
    """
    total_integral = 0
    for cell in valid_cells:
        area, centroid_x, centroid_y = calculate_cell_moments(cell)
        f_value = f(centroid_x, centroid_y)
        integral = f_value * area
        total_integral += integral
    return total_integral
