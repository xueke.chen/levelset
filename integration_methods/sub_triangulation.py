def subdivide_into_triangles(cell):
    """
    Subdivide a cell into two triangles.
    """
    v0, v1, v2, v3 = cell
    triangle1 = [v0, v1, v2]
    triangle2 = [v1, v2, v3]
    return [triangle1, triangle2]

def integrate_over_triangle(f, vertices):
    """
    Integrate f over a triangle.
    """
    x0, y0 = vertices[0]
    x1, y1 = vertices[1]
    x2, y2 = vertices[2]

    area = 0.5 * abs((x1 - x0) * (y2 - y0) - (x2 - x0) * (y1 - y0))
    centroid_x = (x0 + x1 + x2) / 3
    centroid_y = (y0 + y1 + y2) / 3
    f_value = f(centroid_x, centroid_y)
    integral = f_value * area
    return integral

def integrate_using_sub_triangulation(f, valid_cells):
    """
    Integrate using sub-triangulation.
    """
    total_integral = 0
    for cell in valid_cells:
        triangles = subdivide_into_triangles(cell)
        for triangle in triangles:
            integral = integrate_over_triangle(f, triangle)
            total_integral += integral
    return total_integral
