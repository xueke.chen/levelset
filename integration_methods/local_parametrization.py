import numpy as np

def find_boundary_points(X, Y, g, threshold=1e-3):
    """
    Find boundary points where g(x, y) = 0.
    """
    boundary_points = []
    for i in range(X.shape[0]):
        for j in range(Y.shape[1]):
            if abs(g(X[i, j], Y[i, j])) < threshold:  # Points close to the boundary
                boundary_points.append((X[i, j], Y[i, j]))
    return np.array(boundary_points)

def parametrize_boundary(boundary_points):
    """
    Parametrize the boundary using sorted polar angles.
    """
    centroid = np.mean(boundary_points, axis=0)
    angles = np.arctan2(boundary_points[:, 1] - centroid[1], boundary_points[:, 0] - centroid[0])
    sorted_indices = np.argsort(angles)
    sorted_points = boundary_points[sorted_indices]
    t = np.linspace(0, 1, len(sorted_points))
    x_param = sorted_points[:, 0]
    y_param = sorted_points[:, 1]

    def x(t_val):
        return np.interp(t_val, t, x_param)
    
    def y(t_val):
        return np.interp(t_val, t, y_param)
    
    return x, y

def integrate_over_domain(f, X, Y, g, x, y):
    """
    Integrate f(x, y) over the domain where g(x, y) > 0 using local parametrization.
    """
    total_integral = 0
    for i in range(X.shape[0] - 1):
        for j in range(Y.shape[1] - 1):
            cell = [(X[i, j], Y[i, j]), (X[i+1, j], Y[i+1, j]), 
                    (X[i, j+1], Y[i, j+1]), (X[i+1, j+1], Y[i+1, j+1])]
            if all(g(x, y) > 0 for x, y in cell):  # Fully inside the boundary
                xm = (X[i, j] + X[i+1, j] + X[i, j+1] + X[i+1, j+1]) / 4
                ym = (Y[i, j] + Y[i+1, j] + Y[i, j+1] + Y[i+1, j+1]) / 4
                area = (X[i+1, j] - X[i, j]) * (Y[i, j+1] - Y[i, j])
                total_integral += f(xm, ym) * area
            elif any(g(x, y) > 0 for x, y in cell):  # Intersecting the boundary
                intersections = []
                for k in range(len(cell)):
                    x1, y1 = cell[k]
                    x2, y2 = cell[(k + 1) % len(cell)]
                    if g(x1, y1) * g(x2, y2) <= 0:
                        for t_val in np.linspace(0, 1, 100):
                            xt = x(t_val)
                            yt = y(t_val)
                            if min(x1, x2) <= xt <= max(x1, x2) and min(y1, y2) <= yt <= max(y1, y2):
                                intersections.append((xt, yt))
                if len(intersections) == 2:
                    x0, y0 = cell[0]
                    x1, y1 = cell[1]
                    x2, y2 = cell[2]
                    x3, y3 = cell[3]
                    ix1, iy1 = intersections[0]
                    ix2, iy2 = intersections[1]
                    # Split the cell into triangles and integrate over each
                    triangles = [
                        [(x0, y0), (ix1, iy1), (ix2, iy2)],
                        [(x1, y1), (ix1, iy1), (ix2, iy2)],
                        [(x2, y2), (ix1, iy1), (ix2, iy2)],
                        [(x3, y3), (ix1, iy1), (ix2, iy2)]
                    ]
                    for triangle in triangles:
                        xm = np.mean([v[0] for v in triangle])
                        ym = np.mean([v[1] for v in triangle])
                        area = 0.5 * np.abs((triangle[0][0] * (triangle[1][1] - triangle[2][1]) +
                                             triangle[1][0] * (triangle[2][1] - triangle[0][1]) +
                                             triangle[2][0] * (triangle[0][1] - triangle[1][1])))
                        total_integral += f(xm, ym) * area
    return total_integral
