import numpy as np

def create_uniform_grid_3d(a, b, c, d, p, q, num_points):
    """
    Create a uniform 3D grid over the domain [a, b] x [c, d] x [p, q].
    """
    x = np.linspace(a, b, num_points)
    y = np.linspace(c, d, num_points)
    z = np.linspace(p, q, num_points)
    X, Y, Z = np.meshgrid(x, y, z)
    return X, Y, Z

def find_valid_cells_3d(g, X, Y, Z):
    """
    Identify the cells where g(x, y, z) > 0.
    """
    valid_cells = []
    for i in range(X.shape[0] - 1):
        for j in range(X.shape[1] - 1):
            for k in range(X.shape[2] - 1):
                if (g(X[i, j, k], Y[i, j, k], Z[i, j, k]) > 0 or 
                    g(X[i+1, j, k], Y[i+1, j, k], Z[i+1, j, k]) > 0 or 
                    g(X[i, j+1, k], Y[i, j+1, k], Z[i, j+1, k]) > 0 or 
                    g(X[i, j, k+1], Y[i, j, k+1], Z[i, j, k+1]) > 0 or
                    g(X[i+1, j+1, k], Y[i+1, j+1, k], Z[i+1, j+1, k]) > 0 or
                    g(X[i+1, j, k+1], Y[i+1, j, k+1], Z[i+1, j, k+1]) > 0 or
                    g(X[i, j+1, k+1], Y[i, j+1, k+1], Z[i, j+1, k+1]) > 0 or
                    g(X[i+1, j+1, k+1], Y[i+1, j+1, k+1], Z[i+1, j+1, k+1]) > 0):
                    valid_cells.append(((X[i, j, k], Y[i, j, k], Z[i, j, k]), 
                                        (X[i+1, j, k], Y[i+1, j, k], Z[i+1, j, k]), 
                                        (X[i, j+1, k], Y[i, j+1, k], Z[i, j+1, k]), 
                                        (X[i, j, k+1], Y[i, j, k+1], Z[i, j, k+1]),
                                        (X[i+1, j+1, k], Y[i+1, j+1, k], Z[i+1, j+1, k]),
                                        (X[i+1, j, k+1], Y[i+1, j, k+1], Z[i+1, j, k+1]),
                                        (X[i, j+1, k+1], Y[i, j+1, k+1], Z[i, j+1, k+1]),
                                        (X[i+1, j+1, k+1], Y[i+1, j+1, k+1], Z[i+1, j+1, k+1])))
    return valid_cells

def integrate_trilinear_func(func, vertices):
    """
    Approximate the integral over each valid cell using trilinear interpolation.
    """
    x0, y0, z0 = vertices[0]
    x1, y1, z1 = vertices[1]
    x2, y2, z2 = vertices[2]
    x3, y3, z3 = vertices[3]
    x4, y4, z4 = vertices[4]
    x5, y5, z5 = vertices[5]
    x6, y6, z6 = vertices[6]
    x7, y7, z7 = vertices[7]

    f0 = func(x0, y0, z0)
    f1 = func(x1, y1, z1)
    f2 = func(x2, y2, z2)
    f3 = func(x3, y3, z3)
    f4 = func(x4, y4, z4)
    f5 = func(x5, y5, z5)
    f6 = func(x6, y6, z6)
    f7 = func(x7, y7, z7)

    A = f0
    B = f1 - f0
    C = f2 - f0
    D = f4 - f0
    E = f0 - f1 - f2 + f3
    F = f0 - f1 - f4 + f5
    G = f0 - f2 - f4 + f6
    H = f7 - f0 - f1 - f2 - f3 - f4 - f5 - f6

    integral = (A + B/2 + C/2 + D/2 + E/4 + F/4 + G/4 + H/8) * (x2 - x0) * (y1 - y0) * (z3 - z0)
    return integral

def integrate_over_cells_trilinear(func, valid_cells):
    """
    Integrate func over all valid cells using trilinear interpolation.
    """
    total_integral = 0
    for cell in valid_cells:
        integral = integrate_trilinear_func(func, cell)
        total_integral += integral
    return total_integral
